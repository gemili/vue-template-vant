import request from '@/utils/request'

// 查询商家列表
export function getPageTitle () {
    return request({
        url: '/page-title',
        method: 'get'
    })
}
